<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends Controller
{
    // rejestracja przez wbudowany system autoryzacji symfony
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!(is_null($user))) {
            return $this->redirectToRoute('lists');
        }

        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        // przy rejestracji użytkownikowi generowany jest hash, jest on ważny 7 dni. Pozwala na aktywacje konta.
        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $date->modify('+ 7 days');
            $link = "https://buyme-shopping.com/registerconfirm/";
            $end = (string)rand(100, 9999);
            $hash = hash('sha256', $user->getName().$user->getSurname().$user->getEmail().$end);

            $user->setHash($hash);
            $user->setHashDate($date);

            $user->setIsActive(1);
            $user->setStatus('N');
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $em->persist($user);
            $em->flush();

            $this->sendRegisterInfo($user->getName(), $user->getEmail(), $mailer, $link.$hash);

            return $this->redirectToRoute('thx_register');
        }

        return $this->render('shopping_list/register.html.twig', array(
            'form' => $form->createView()
        ));
    }

    // akcja odpowiedzialna za aktywacje konta. sprawdzana jest data hasha, jesli nie jest ważny generowany jest nowy i wysyłany na maila
    // jeśli hash jest ważny zmieniony zostaje status konta
    /**
     * @Route("/registerconfirm/{hash}", name="register_confirm")
     * @param $hash
     * @param Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function registerConfirmAction($hash, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['hash' => $hash]);

        if (empty($user)) {
            return $this->redirectToRoute("login");
        }

        $currentDate = new \DateTime();
        $interval = $currentDate->diff($user->getHashDate());

        if ($interval->d > 7) {
            $end = (string)rand(100, 9999);
            $hash2 = hash('sha256', $user->getName().$user->getSurname().$user->getEmail().$end);
            $date = new \DateTime();
            $date->modify('+ 7 days');
            $link = "https://buyme-shopping.com/registerconfirm/";
            $user->setHash($hash2);
            $user->setHashDate($date);
            $em->persist($user);
            $em->flush();
            $this->addFlash("message", "Acount not activated. Activation email send again");
            $this->sendRegisterInfo($user->getName(), $user->getEmail(), $mailer, $link.$hash2);
            return $this->redirectToRoute('login');

        } else {
            $user->setStatus('A');
            $user->setHash(NULL);
            $user->setHashDate(NULL);
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('thx_activation');
        }
    }

    // akcja odpowiedzialna za uaktualnienie profilu po zalogowaniu.
    /**
     * @Route("/update", name="user_profil_update")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profilUpdateAction(Request $request)
    {
        $logedUser = $this->getUser();

        $mail = $logedUser->getEmail();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['email' => $mail]);

        $name = $request->get('_name');
        $surname = $request->get('_surname');
        $city = $request->get('_city');

        $user->setName($name);
        $user->setSurname($surname);
        $user->setCity($city);

        if (!(empty($name)) || !(empty($surname)) || !(empty($city))) {
            $em->persist($user);
            $em->flush();

            $this->addFlash("message", "Your information updated.");
        }

        return $this->render('shopping_list/update_profile.html.twig');
    }

    /**
     * @param $name
     * @param $email
     * @param $mailer
     * @param $hash
     */
    function sendRegisterInfo($name, $email, $mailer, $hash)
    {
        $message = (new \Swift_Message('Potwierdzenie rejestracji w Shopping List'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/registration.html.twig',
                    array('name' => $name, 'hash' => $hash, 'message' => '')
                ),
                'text/html'
            );

        $mailer->send($message);
    }
}
