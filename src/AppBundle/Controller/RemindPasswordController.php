<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\RemindPasswordType;
use Swift_Mailer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RemindPasswordController extends Controller
{
    // akcja z formularzem odpowiedzialnym za reset hasła
    /**
     * @Route("/reset", name="reset_password")
     */
    public function remindPasswordAction()
    {
        $user = $this->getUser();
        if (!(is_null($user))) {
            return $this->redirectToRoute('lists');
        }

        return $this->render('shopping_list/reset.html.twig', array(
            'msg' => ''
        ));
    }

     // akcja odpowiedzialna za resetowania hasła, jeśli użytkownik ma ważnego hasha i aktywowane konto, wysyłany jest link do formularza
     // w którym wprowadzane jest nowe hasło, jeśli użytkownik nie aktywował konta ponownie jest wysyłany link do aktywacji konta
    /**
     * @Route("/resetpassword", name="reset_password_send")
     * @param Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remindPasswordSendAction(\Swift_Mailer $mailer)
    {
        $link = "https://buyme-shopping.com/newpassword/";

        $mail = $_POST['_email'];
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['email' => $mail]);

        if (empty($user)) {
            $this->addFlash("message", "Account not found");
            return $this->redirectToRoute('reset_password');
        }

        $end = (string)rand(100, 9999);
        $hash = hash('sha256', $user->getName().$user->getSurname().$user->getEmail().$end);

        $userStatus = $user->getStatus();

        $date = new \DateTime();
        $date->modify('+ 7 days');

        if ($userStatus == "N") {
            $link = "https://buyme-shopping.com/registerconfirm/";
            $user->setHash($hash);
            $user->setHashDate($date);
            $em->persist($user);
            $em->flush();

            $this->addFlash("message", "Account not activated. You activation email send again.");
            $this->sendRegisterInfo($user->getName(), $user->getEmail(), $mailer, $link.$hash);
        } else {
            $user->setHash($hash);
            $user->setHashDate($date);
            $em->persist($user);
            $em->flush();
            $this->addFlash("message", "Restart link was send to Your email address. ");
            $this->sendLinkToResetPassword($user->getName(), $user->getEmail(), $link.$hash, $mailer);
        }

        return $this->redirectToRoute('reset_password');
    }

    // akcja która odpowiada za wprowadzenie nowego hasła, jest sprawdzana data hasha, jeśli hash ważny dostępny jest formularz.
    // jeśli hash wygasł przenoszenie do formularza by wysłać nowy link
    /**
     * @Route("/newpassword/{hash}", name="new_password")
     * @param Request $request
     * @param $hash
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newPasswordAction(Request $request, $hash, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(RemindPasswordType::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['hash' => $hash]);

        $currentDate = new \DateTime();
        $interval = $currentDate->diff($user->getHashDate());

        if ($interval->d > 7) {
            $this->addFlash("message", "Activation link not found. Send again.");
            return $this->redirectToRoute('reset_password');
        } else {
            if ($form->isSubmitted() && $form->isValid()) {
                $password = $form['password']->getData();
                $user->setPassword($encoder->encodePassword($user, $password));
                $user->setHash(NULL);
                $user->setHashDate(NULL);
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('login');
            }
        }

        return $this->render('shopping_list/newpassword.html.twig', array(
            'form' => $form->createView()
        ));
    }

    function sendLinkToResetPassword($name, $email, $hash, $mailer)
    {
        $message = (new \Swift_Message('Zresetuj hasło'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/remindpassword.html.twig',
                    array('name' => $name, 'hash' => $hash)
                ),
                'text/html'
            );

        $mailer->send($message);
    }

    function sendRegisterInfo($name, $email, $mailer, $hash)
    {
        $message = (new \Swift_Message('Potwierdzenie rejestracji w BuyMe!'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/registration.html.twig',
                    array('name' => $name, 'hash' => $hash, 'message' => '')
                ),
                'text/html'
            );

        $mailer->send($message);
    }
}
