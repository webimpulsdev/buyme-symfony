<?php

namespace ApiRestBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Swift_Mailer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterApiController extends FOSRestController
{
    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      {
     *        "email_address": "pawel.lomaski@example.com",
     *        "password": "lomaskI96*"
     *      }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Register new user of website and mobile application",
     *     resource = true,
     *     section = "Registration",
     *     statusCodes = {
     *         201 = "Returned when registration is finalized successfully",
     *         400 = "Returned when both 'email_address' and 'password' aren't filled",
     *         409 = "Returned when user with given 'email_address' already exists",
     *     },
     *     requirements = {
     *     {
     *         "name" = "email_address",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new user email"
     *     },
     *     {
     *         "name" = "password",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new user password"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Post("/v1/register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param Swift_Mailer $mailer
     * @return View
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder, Swift_Mailer $mailer)
    {
        $email_address = $request->get('email_address');
        $password = $request->get('password');

        $em = $this->getDoctrine()->getManager();
        $users = $this->getDoctrine()->getRepository(User::class)->loadUserByUsername($email_address);

        if (!is_null($users))
        {
            return View::create("Użytkownik o takim adresie email już istnieje!", Response::HTTP_CONFLICT);
        }

        if (empty($email_address) || empty($password))
        {
            return View::create("Prosze uzupełnić wszystkie pola!", Response::HTTP_BAD_REQUEST);
        }

        if (!filter_var($email_address, FILTER_VALIDATE_EMAIL))
        {
            return View::create("Podany email ma nieprawidłowy format!", Response::HTTP_BAD_REQUEST);
        }

        $user = new User();

        $date = new \DateTime();
        $date->modify('+ 7 days');

        $end = (string)rand(100, 9999);
        $hash = hash('sha256', $user->getName().$user->getSurname().$user->getEmail().$end);
        $link = "https://buyme-shopping.com/registerconfirm/";

        $user->setHash($hash);
        $user->setHashDate($date);
        $user->setEmail($email_address);

        $user->setIsActive(1);
        $user->setStatus('N');
        $user->setPassword($encoder->encodePassword($user, $password));

        $em->persist($user);
        $em->flush();

        $this->sendRegisterInfo($user->getName(), $user->getEmail(), $mailer, $link.$hash);

        return View::create(['email_address' => $email_address, 'password' => $password], Response::HTTP_CREATED);
    }

    /**
     * @param $name
     * @param $email
     * @param $mailer
     * @param $hash
     */
    function sendRegisterInfo($name, $email, $mailer, $hash)
    {
        $message = (new \Swift_Message('Potwierdzenie rejestracji w Shopping List'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/registration.html.twig',
                    array('name' => $name, 'hash' => $hash, 'message' => '')
                ),
                'text/html'
            );

        $mailer->send($message);
    }
}