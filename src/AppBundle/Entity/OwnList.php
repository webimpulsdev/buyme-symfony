<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OwnList
 *
 * @ORM\Table(name="own_list", indexes={@ORM\Index(name="own_list_fk0", columns={"id_list"}), @ORM\Index(name="own_list_fk1", columns={"id_user"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OwnListRepository");
 */
class OwnList
{
    /**
     * @var string
     *
     * @ORM\Column(name="permission", type="string", length=4, nullable=false)
     *
     */
    private $permission;

    /**
     * @return string
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @param string $permission
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param Lista $idList
     */
    public function setIdList($idList)
    {
        $this->idList = $idList;
    }

    /**
     * @param User $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @var \AppBundle\Entity\Lista
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lista", inversedBy="ownList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id")
     * })
     */
    private $idList;

    /**
     * @return Lista
     */
    public function getIdList()
    {
        return $this->idList;
    }

    /**
     * @return User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;


}

