<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type as JMS;

/**
 * Lista
 *
 * @ORM\Table(name="lista")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ListaRepository")
 */
class Lista
{
    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=250, nullable=true)
     */
    private $hash;

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="list_status", type="string", length=10, nullable=false)
     */
    private $listStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="list_name", type="string", length=20, nullable=false)
     */
    private $listName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="OwnList", mappedBy="idList", cascade={"persist"})
     */
    private $ownList;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="idList", cascade={"persist"})
     */
    private $product;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetimetz")
     */
    private $data;

    /**
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param \DateTime $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return Lista
     */
    public function addProduct(\AppBundle\Entity\Product $product)
    {
        $this->product[] = $product;
        $product->setIdList($this);
        return $this;
    }

    /**
     * Remove product
     *
     * @param \AppBundle\Entity\Product $product
     */
    public function removeProduct(\AppBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Get product
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return mixed
     */
    public function getOwnList()
    {
        return $this->ownList;
    }

    /**
     * @return string
     */
    public function getListStatus()
    {
        return $this->listStatus;
    }

    /**
     * @param string $listStatus
     */
    public function setListStatus($listStatus)
    {
        $this->listStatus = $listStatus;
    }

    /**
     * @return string
     */
    public function getListName()
    {
        return $this->listName;
    }

    /**
     * @param string $listName
     */
    public function setListName($listName)
    {
        $this->listName = $listName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}

