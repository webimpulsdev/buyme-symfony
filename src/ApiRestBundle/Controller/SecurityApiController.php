<?php

namespace ApiRestBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityApiController extends FOSRestController
{
    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      {
     *        "email_address": "adam.bratek@example.com"
     *      }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Reset password for user of website and mobile application",
     *     resource = true,
     *     section = "Registration",
     *     statusCodes = {
     *         200 = "Returned when user doesn't exists",
     *         201 = "Returned when user didn't activated his account",
     *         200 = "Returned when password is reseted successfully",
     *     },
     *     requirements = {
     *     {
     *         "name" = "email_address",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "user email to send password reset link"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Post("/v1/reset_password")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return View
     */
    public function resetPasswordAction(Request $request, \Swift_Mailer $mailer)
    {
        $email_address = $request->get('email_address');
        $em = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email_address]);

        if (empty($email_address) || is_null($user))
        {
            return View::create(['email_address' => $email_address], Response::HTTP_OK);
        }

        $end = (string)rand(100, 9999);
        $hash = hash('sha256', $user->getName().$user->getSurname().$user->getEmail().$end);
        $linkToNewPassword = "https://buyme-shopping.com/newpassword/";

        $date = new \DateTime();
        $date->modify('+ 7 days');

        if ($user->getStatus() == 'N')
        {
            $link = "https://buyme-shopping.com/registerconfirm/";
            $user->setHash($hash);
            $user->setHashDate($date);

            $em->persist($user);
            $em->flush();

            $this->sendRegisterInfo($user->getName(), $user->getEmail(), $mailer, $link.$hash);

            return View::create(['email_address' => $email_address], Response::HTTP_CREATED);
        }

        $user->setHash($hash);
        $user->setHashDate($date);

        $em->persist($user);
        $em->flush();

        $this->sendLinkToResetPassword($user->getName(), $user->getEmail(), $linkToNewPassword.$hash, $mailer);

        return View::create(['email_address' => $email_address], Response::HTTP_OK);
    }

    function sendLinkToResetPassword($name, $email, $hash, $mailer)
    {
        $message = (new \Swift_Message('Zresetuj hasło'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/remindpassword.html.twig',
                    array('name' => $name, 'hash' => $hash)
                ),
                'text/html'
            );

        $mailer->send($message);
    }

    function sendRegisterInfo($name, $email, $mailer, $hash)
    {
        $message = (new \Swift_Message('Potwierdzenie rejestracji w BuyMe!'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/registration.html.twig',
                    array('name' => $name, 'hash' => $hash, 'message' => '')
                ),
                'text/html'
            );

        $mailer->send($message);
    }
}