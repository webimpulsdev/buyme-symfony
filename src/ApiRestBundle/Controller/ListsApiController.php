<?php

namespace ApiRestBundle\Controller;

use AppBundle\Entity\Lista;
use AppBundle\Entity\OwnList;
use AppBundle\Entity\Product;
use AppBundle\Entity\SharedList;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class ListsApiController extends FOSRestController
{
    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *     [
     *       {
     *         "id": 1,
     *         "listName": "Lista nr.1",
     *         "data": "2018-09-15T09:57:53+0200",
     *         "limited_products": "mleko, jogurt, cola, sok jabłkowy, chipsy",
     *         "checked_products_count": 1,
     *         "products_count": 5
     *       },
     *       {
     *         "id": 2,
     *         "listName": "Lista nr.2",
     *         "data": "2018-09-15T11:04:13+0200",
     *         "limited_products": "banan, arbuz, szynka, ser żółty",
     *         "checked_products_count": 1,
     *         "products_count": 4
     *       },
     *       {
     *         "id": 3,
     *         "listName": "Lista nr.3",
     *         "data": "2018-09-15T13:43:07+0200",
     *         "limited_products": "boczek, cebula, czerwone wino, gorący bulion, koncentrat pomidorowy...",
     *         "checked_products_count": 0,
     *         "products_count": 13
     *       }
     *     ]
     *
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Get list of product lists (without products)",
     *     resource = true,
     *     section = "Lists",
     *     statusCodes = {
     *         200 = "Returned when list or lists are loaded successfully",
     *         404 = "Returned when list type don't exists",
     *     },
     *     parameters = {
     *     {
     *         "name" = "_page",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "which page to choose"
     *     },
     *     {
     *         "name" = "_page_limit",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "set results limit on one page"
     *     },
     *     {
     *         "name" = "_type",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "which list type you choose (1 - aktywna, 2 - archiwum, 3 - współdzielona)"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Get("/v1/lists")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function showListsAction(Request $request, UserInterface $user)
    {
        $type = $request->get('_type');
        $serializer = $this->get('serializer');
        $checkedProductsCount = 0;
        $productsCount = 0;

        if ($type == 1)
        {
            $lists = $this->getDoctrine()->getRepository(Lista::class)->findUserListsByUser($user->getId(), 'aktywna');

            foreach ($lists as $list)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsFromUserList($list['id']);

                foreach ($productsOnList as $formatedProducts)
                {
                    $exploded = explode(', ', $formatedProducts['products']);

                    if (count($exploded) > 5)
                    {
                        $shrinked = array_slice($exploded, 0, 5);
                        $imploded = implode(', ', $shrinked);

                        $lists[array_search($list, $lists)]['limited_products'] = $imploded . '...';
                    }
                    else
                    {
                        $lists[array_search($list, $lists)]['limited_products'] = implode(', ', $exploded);
                    }
                }
            }

            foreach ($lists as $listToCountProducts)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($listToCountProducts['id']);

                foreach ($productsOnList as $product)
                {
                    if ($product['productStatus'] == true)
                    {
                        $checkedProductsCount++;
                    }
                }

                $lists[array_search($listToCountProducts, $lists)]['checked_products_count'] = $checkedProductsCount;
                $checkedProductsCount = 0;
            }

            foreach ($lists as $listToCountProducts)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($listToCountProducts['id']);

                foreach ($productsOnList as $product)
                {
                    $productsCount++;
                }

                $lists[array_search($listToCountProducts, $lists)]['products_count'] = $productsCount;
                $productsCount = 0;
            }

            foreach ($lists as $formatList)
            {
                unset($lists[array_search($formatList, $lists)]['products']);
            }
        }
        elseif ($type == 2)
        {
            $lists = $this->getDoctrine()->getRepository(Lista::class)->findUserListsByUser($user->getId(), 'archiwum');

            foreach ($lists as $list)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsFromUserList($list['id']);

                foreach ($productsOnList as $formatedProducts)
                {
                    $exploded = explode(', ', $formatedProducts['products']);

                    if (count($exploded) > 5)
                    {
                        $shrinked = array_slice($exploded, 0, 5);
                        $imploded = implode(', ', $shrinked);

                        $lists[array_search($list, $lists)]['limited_products'] = $imploded . '...';
                    }
                    else
                    {
                        $lists[array_search($list, $lists)]['limited_products'] = implode(', ', $exploded);
                    }
                }
            }

            foreach ($lists as $listToCountProducts)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($listToCountProducts['id']);

                foreach ($productsOnList as $product)
                {
                    if ($product['productStatus'] == true)
                    {
                        $checkedProductsCount++;
                    }
                }

                $lists[array_search($listToCountProducts, $lists)]['checked_products_count'] = $checkedProductsCount;
                $checkedProductsCount = 0;
            }

            foreach ($lists as $listToCountProducts)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($listToCountProducts['id']);

                foreach ($productsOnList as $product)
                {
                    $productsCount++;
                }

                $lists[array_search($listToCountProducts, $lists)]['products_count'] = $productsCount;
                $productsCount = 0;
            }

            foreach ($lists as $formatList)
            {
                unset($lists[array_search($formatList, $lists)]['products']);
            }
        }
        elseif ($type == 3)
        {
            $lists = $this->getDoctrine()->getRepository(Lista::class)->findUserSharedLists($user->getId());

            foreach ($lists as $list)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsFromUserList($list['id']);

                foreach ($productsOnList as $formatedProducts)
                {
                    $exploded = explode(', ', $formatedProducts['products']);

                    if (count($exploded) > 5)
                    {
                        $shrinked = array_slice($exploded, 0, 5);
                        $imploded = implode(', ', $shrinked);

                        $lists[array_search($list, $lists)]['limited_products'] = $imploded . '...';
                    }
                    else
                    {
                        $lists[array_search($list, $lists)]['limited_products'] = implode(', ', $exploded);
                    }
                }
            }

            foreach ($lists as $listToCountProducts)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($listToCountProducts['id']);

                foreach ($productsOnList as $product)
                {
                    if ($product['productStatus'] == true)
                    {
                        $checkedProductsCount++;
                    }
                }

                $lists[array_search($listToCountProducts, $lists)]['checked_products_count'] = $checkedProductsCount;
                $checkedProductsCount = 0;
            }

            foreach ($lists as $listToCountProducts)
            {
                $productsOnList = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($listToCountProducts['id']);

                foreach ($productsOnList as $product)
                {
                    $productsCount++;
                }

                $lists[array_search($listToCountProducts, $lists)]['products_count'] = $productsCount;
                $productsCount = 0;
            }

            foreach ($lists as $formatList)
            {
                unset($lists[array_search($formatList, $lists)]['products']);
            }
        }
        else
        {
            return View::create('Podany typ nie istnieje!', Response::HTTP_NOT_FOUND);
        }


        $page = $request->get('_page');
        $pageLimit = $request->get('_page_limit');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($lists, $page, $pageLimit);

        $result = $serializer->normalize($pagination, 'json');

        return View::create($result, Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      ""
     *
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Update list status to 'archiwum'",
     *     resource = true,
     *     section = "Lists",
     *     statusCodes = {
     *         200 = "Returned when list is successfully stored in archive",
     *         401 = "Returned when list of given id don't exists, its status is 'archiwum' or the owner don't match",
     *     },
     *     parameters = {
     *     {
     *         "name" = "_id",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "list of which id to archivize"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Put("/v1/archivize_list")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function archivizeListAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id');

        $em = $this->getDoctrine()->getManager();
        $listToArchivize = $this->getDoctrine()->getRepository(Lista::class)->find($id);

        if (empty($listToArchivize) || $listToArchivize->getListStatus() == 'archiwum')
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($listToArchivize->getId());

        if ($checkList['id'] != $user->getId())
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $listToArchivize->setListStatus('archiwum');

        $em->persist($listToArchivize);
        $em->flush();

        return View::create("", Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      ""
     *
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Add new shopping list",
     *     resource = true,
     *     section = "Lists",
     *     statusCodes = {
     *         200 = "Returned when list is successfully added",
     *         400 = "Returned when list name wasn't provided",
     *     },
     *     parameters = {
     *     {
     *         "name" = "_list_name",
     *         "dataType" = "string",
     *         "required" = "true",
     *         "description" = "name of list to add"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Post("/v1/add_list")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function addListAction(Request $request, UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager();

        $listName = $request->get('_list_name');

        if (empty($listName))
        {
            return View::create("", Response::HTTP_BAD_REQUEST);
        }

        $listToAdd = new Lista();
        $listToAdd->setListName($listName);
        $listToAdd->setData(new \DateTime());
        $listToAdd->setListStatus('aktywna');

        $email = $user->getEmail();
        $end = (string)rand(100, 9999);
        $hash = hash('sha256', $email.$end);

        $listToAdd->setHash($hash);

        $em->persist($listToAdd);
        $em->flush();

        $ownListToAdd = new OwnList();
        $ownListToAdd->setIdList($listToAdd);
        $ownListToAdd->setPermission('edit');
        $ownListToAdd->setIdUser($user);

        $em->persist($ownListToAdd);
        $em->flush();

        return View::create("", Response::HTTP_CREATED);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      ""
     *
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Update list status to 'aktywna'",
     *     resource = true,
     *     section = "Lists",
     *     statusCodes = {
     *         200 = "Returned when list is successfully stored in active lists",
     *         401 = "Returned when list of given id don't exists, its status is 'aktywna' or the owner don't match",
     *     },
     *     parameters = {
     *     {
     *         "name" = "_id",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "list of which id to store in active lists"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Put("/v1/restore_list")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function restoreListAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id');

        $em = $this->getDoctrine()->getManager();
        $listToArchivize = $this->getDoctrine()->getRepository(Lista::class)->find($id);

        if (empty($listToArchivize) || $listToArchivize->getListStatus() == 'aktywna')
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($listToArchivize->getId());

        if ($checkList['id'] != $user->getId())
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $listToArchivize->setListStatus('aktywna');

        $em->persist($listToArchivize);
        $em->flush();

        return View::create("", Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      ""
     *
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Delete a list of given id",
     *     resource = true,
     *     section = "Lists",
     *     statusCodes = {
     *         200 = "Returned when list is successfully deleted",
     *         401 = "Returned when list of given id don't exists, it isn't related to shared lists or the owner don't match",
     *     },
     *     parameters = {
     *     {
     *         "name" = "_id",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "list of which id to delete"
     *     }
     *     },
     *     views = {"v1"}
     * )
     *
     * @Delete("/v1/delete_list")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function deleteListAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id');

        $em = $this->getDoctrine()->getManager();

        $listToDelete = $this->getDoctrine()->getRepository(Lista::class)->find($id);

        if (empty($listToDelete))
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($listToDelete->getId());
        $sharedListCheck = $this->getDoctrine()->getRepository(SharedList::class)->findSharedListByIdList($id);

        if (empty($sharedListCheck))
        {
            if ($checkList['id'] != $user->getId())
            {
                return View::create("", Response::HTTP_UNAUTHORIZED);
            }

            $listToDelete->setListStatus('deleted');

            $em->persist($listToDelete);
            $em->flush();

            return View::create("", Response::HTTP_OK);
        }

        if ($sharedListCheck['id_user'] != $user->getId() )
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $listToDelete->setListStatus('deleted');

        $em->persist($listToDelete);
        $em->flush();

        return View::create("", Response::HTTP_OK);
    }
}
