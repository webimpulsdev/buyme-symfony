<?php
/**
 * Created by PhpStorm.
 * User: bate
 * Date: 17.07.2018
 * Time: 10:58
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Lista;
use AppBundle\Entity\OwnList;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class OwnListRepository extends EntityRepository
{
    public function findOwnListUserById($id)
    {
        $fields = ['u.id'];

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select($fields)
            ->from(OwnList::class, 'o')
            ->innerJoin(Lista::class, 'l', 'WITH', 'l.id = o.idList')
            ->innerJoin(User::class, 'u', 'WITH', 'u.id = o.idUser')
            ->where('o.idList = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $qb;
    }

    public function findOwnListById($id)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')
            ->from(OwnList::class, 'o')
            ->where('o.idList = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $qb;
    }
}