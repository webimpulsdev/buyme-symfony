<?php
/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 2018-07-16
 * Time: 11:03
 */

namespace AppBundle\Repository;

use AppBundle\Entity\OwnList;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Lista;
use Doctrine\ORM\Query;

class ListaRepository extends EntityRepository
{
    public function loadUserList()
    {
        $qb = $this->createQueryBuilder('n');
        return $qb
            ->select('n')
            ->where('n.id = :id')
            ->setParameters([
                'id' => 1,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllLists()
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from(Lista::class, 'a')
            ->getQuery()
            ->getArrayResult();

        return $qb;
    }

    public function findListById($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('l')
            ->from(Lista::class, 'l')
            ->where('l.id = ' . $id)
            ->getQuery()
            ->getResult();

        return $qb;
    }

    public function findUserListById($id)
    {
        $fields = ['l.id', 'l.listName', 'l.data'];

        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select($fields)
            ->from(Lista::class, 'l')
            ->where('l.id = ' . $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $qb;
    }

    public function findUserListsByUser($id, $status)
    {
        $fields = ['l.id', 'l.listName', 'l.data'];

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select($fields)
            //->addSelect("GROUP_CONCAT(p.productName ORDER BY p.productName SEPARATOR ', ') AS products")
            ->from('AppBundle:Lista', 'l')
            ->innerJoin('AppBundle:OwnList', 'o', 'WITH', 'l.id = o.idList')
            ->innerJoin('AppBundle:User', 'u', 'WITH', 'o.idUser = u.id')
            //->innerJoin('AppBundle:Product', 'p', 'WITH', 'l.id = p.idList')
            ->where('u.id = :id')
            ->andWhere("l.listStatus = :status")
            ->groupBy('l.id')
            ->setParameters(['id' => $id, 'status' => $status])
            ->getQuery()
            ->getResult();

        return $qb;
    }

    public function findUserSharedLists($id)
    {
        $fields = ['l.id', 'l.listName', 'l.data'];

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select($fields)
            ->from('AppBundle:Lista', 'l')
            ->innerJoin('AppBundle:SharedList', 's', 'WITH', 'l.id = s.idList')
            ->where('s.idUser = :id')
            ->groupBy('l.id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        return $qb;
    }
}
