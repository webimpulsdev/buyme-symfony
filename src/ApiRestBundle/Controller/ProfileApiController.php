<?php

namespace ApiRestBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class ProfileApiController extends FOSRestController
{
    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      {
     *        "name": "Jan",
     *        "surname": "Kowalski",
     *        "city": "Warszawa",
     *        "email": "jan.kowalski@example.com",
     *        "status": "N"
     *      }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Get a profile details of logged user",
     *     resource = true,
     *     section = "Profile",
     *     statusCodes = {
     *         200 = "Returned when profile is loaded successfully",
     *         404 = "Returned when user profile doesn't exist",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Get("/v1/profile_show")
     * @param UserInterface $user
     * @return View
     */
    public function profileShowAction(UserInterface $user)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->loadProfileByLoggedUser($user->getId());

        if (empty($user))
        {
            return View::create("", Response::HTTP_NOT_FOUND);
        }

        return View::create($user, Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      {
     *        "name": "Paweł",
     *        "surname": "Łomaski",
     *        "city": "Kraków"
     *      }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Update a profile details of logged user",
     *     resource = true,
     *     requirements = {
     *     {
     *         "name" = "name",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new user name"
     *     },
     *     {
     *         "name" = "surname",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new user surname"
     *     },
     *     {
     *         "name" = "city",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new user city name"
     *     }
     *     },
     *     section = "Profile",
     *     statusCodes = {
     *         200 = "Returned when profile is updated successfully",
     *         400 = "Returned when user has inactive account",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Put("/v1/profile_update")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function profileUpdateAction(Request $request, UserInterface $user)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $city = $request->get('city');

        $em = $this->getDoctrine()->getManager();

        $userProfile = $em->getRepository(User::class)->find($user->getId());

        if ($userProfile->getStatus() == 'N')
        {
            return View::create(['name' => $name, 'surname' => $surname, 'city' => $city], Response::HTTP_BAD_REQUEST);
        }

        $userProfile->setName($name);
        $userProfile->setSurname($surname);
        $userProfile->setCity($city);

        $em->persist($userProfile);
        $em->flush();

        return View::create(['name' => $name, 'surname' => $surname, 'city' => $city], Response::HTTP_OK);
    }
}