<?php
/**
 * Created by PhpStorm.
 * User: piotr-rybski
 * Date: 18.09.18
 * Time: 13:08
 */

namespace AppBundle\Controller;

use AppBundle\Entity\TemplateLists;
use AppBundle\Entity\TemplateProducts;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DatabaseController extends Controller
{
    /**
     * @Route("/database/add_template", name="_database_add_template")
     * @param Request $request
     * @return Response
     */
    public function addTemplateToDatabase(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $templateName = $request->get('_template_name');

        if (empty($templateName))
        {
            return new Response("Podaj parametr GET _template_name!");
        }

        $template = new TemplateLists();
        $template->setTemplateName($templateName);
        $template->setDate(new \DateTime());

        $em->persist($template);
        $em->flush();

        return new Response("Template of name " . $templateName . " added!");
    }

    /**
     * @Route("/database/add_product", name="_database_add_product")
     * @param Request $request
     * @return Response
     */
    public function addProductsToTemplate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $templateName = $request->get("_template_name");

        $quantity = $request->get('_quantity');
        $unit = $request->get('_unit');
        $productName = $request->get('_product_name');

        $templates = $this->getDoctrine()->getRepository(TemplateLists::class)->findAll();
        $templateProduct = new TemplateProducts();

        foreach ($templates as $template)
        {
            if ($template->getTemplateName() == $templateName)
            {
                $templateProduct->setTemplate($template);
                $templateProduct->setQuantity($quantity);
                $templateProduct->setUnit($unit);
                $templateProduct->setProductName($productName);

                $em->persist($templateProduct);
                $em->flush();
            }
        }

        return new Response("New product of name " . $templateProduct->getProductName() . " was added!");
    }
}