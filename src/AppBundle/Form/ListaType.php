<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listName', TextType::class,
                ['label' => false, 'attr' => array(
                'placeholder' => 'List name: ',
                'maxlength'=>"40"
            )])
            ->add('product', CollectionType::class, [
                'entry_type' => ProductType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('status', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-outline-secondary'
                ]
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning saveChanges',
                    'id' => 'saveRecord' ,
                ]
            ])
            ->add('duplicate', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning',
                    'id' => 'duplicateRecord'
                ]
            ])
            ->add('delete', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning',
                    'id' => 'deleteRecord'
                ]
            ])
            ->add('close_and_move', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning',
                    'id' => 'closeAndMoveRecord'
                ]
            ])
            ->add('to_template', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning',
                    'id' => 'saveToTemplate'
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildArchive(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Archiwizuj', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Lista'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_lista';
    }
}
