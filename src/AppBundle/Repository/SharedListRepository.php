<?php
/**
 * Created by PhpStorm.
 * User: Komp
 * Date: 07.09.2018
 * Time: 09:16
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Lista;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\SharedList;
use Doctrine\ORM\NonUniqueResultException;

class SharedListRepository extends EntityRepository
{
    public function findUserIdUser($idUser, $idList)
    {
        $qb = $this->getEntityManager();

        return $qb->createQueryBuilder()
                ->select('s')
                ->from('AppBundle:SharedList', 's')
                ->where('s.idList = :idList')
                ->andWhere('s.idUser = :idUser')
                ->setParameters(['idList' => $idList, 'idUser' => $idUser])
                ->getQuery()
                ->getOneOrNullResult();
    }

    public function findSharedListByIdList($id)
    {
        $qb = $this->getEntityManager();

        $fields = ['l.id AS id_list', 'u.id AS id_user'];

        return $qb->createQueryBuilder()
            ->select($fields)
            ->from('AppBundle:SharedList', 's')
            ->innerJoin(Lista::class, 'l', 'WITH', 's.idList = l.id')
            ->innerJoin(User::class, 'u', 'WITH', 's.idUser = u.id')
            ->where('l.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOwnerOfSharedListByIdList($id)
    {
        $qb = $this->getEntityManager();

        $fields = ['l.id AS id_list', 'u.id AS id_owner'];

        return $qb->createQueryBuilder()
            ->select($fields)
            ->from('AppBundle:SharedList', 's')
            ->innerJoin(Lista::class, 'l', 'WITH', 's.idList = l.id')
            ->innerJoin(User::class, 'u', 'WITH', 's.idOwner = u.id')
            ->where('l.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findSharedListByUserAndListStatus($idUser)
    {
        $qb = $this->getEntityManager();

        return $qb->createQueryBuilder()
            ->select('s')
            ->from('AppBundle:SharedList', 's')
            ->innerJoin(Lista::class, 'l', 'WITH', 's.idList = l.id')
            ->innerJoin(User::class, 'u', 'WITH', 's.idUser = u.id')
            ->where("l.listStatus != 'deleted'")
            ->andWhere('u.id = :idUser')
            ->setParameter('idUser', $idUser)
            ->getQuery()
            ->getResult();
    }

    public function findSharedListByIdListAll($id)
    {
        $qb = $this->getEntityManager();

        return $qb->createQueryBuilder()
            ->select('s')
            ->from('AppBundle:SharedList', 's')
            ->innerJoin(Lista::class, 'l', 'WITH', 's.idList = l.id')
            ->innerJoin(User::class, 'u', 'WITH', 's.idUser = u.id')
            ->where('l.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}