<?php
/**
 * Created by PhpStorm.
 * User: bate
 * Date: 26.07.2018
 * Time: 11:01
 */

namespace Classes;

class SharedLists
{
    private $nameList;
    private $date;
    private $ownerName;
    private $ownerSurname;

    /**
     * @return mixed
     */
    public function getNameList()
    {
        return $this->nameList;
    }

    /**
     * @param mixed $nameList
     */
    public function setNameList($nameList)
    {
        $this->nameList = $nameList;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * @param mixed $ownerName
     */
    public function setOwnerName($ownerName)
    {
        $this->ownerName = $ownerName;
    }

    /**
     * @return mixed
     */
    public function getOwnerSurname()
    {
        return $this->ownerSurname;
    }

    /**
     * @param mixed $ownerSurname
     */
    public function setOwnerSurname($ownerSurname)
    {
        $this->ownerSurname = $ownerSurname;
    }


}