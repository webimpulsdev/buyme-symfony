<?php
/**
 * Created by PhpStorm.
 * User: Komp
 * Date: 07.09.2018
 * Time: 08:59
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SharedListType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('save_permissions', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning',
                    'id' => 'savePermissions'
                ]
            ])
            ->add('permissions', ChoiceType::class, array('label' => false, 'attr' => array('placeholder' => 'Uprawnienia'),
                'choices' =>  array(
                    'edit' => 'edit',
                    'view' => 'view',
                    'shopping' => 'shopping'
                )
            ))
            ->add('email_to_share',
                TextType::class,
                ['label' => false,
                    'attr' => array(
                        'class' => 'required_false',
                        'hidden' => 'hidden'
                    )]
            )
        ;
    }



}