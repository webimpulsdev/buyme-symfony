<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SharedList
 *
 * @ORM\Table(name="shared_list", indexes={@ORM\Index(name="shared_list_fk0", columns={"id_list"}), @ORM\Index(name="shared_list_fk1", columns={"id_user"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SharedListRepository")
 */
class SharedList
{
    /**
     * @var string
     *
     * @ORM\Column(name="permission", type="string", length=8, nullable=false)
     */
    private $permission;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_owner", referencedColumnName="id")
     * })
     */
    private $idOwner;

    /**
     * @return User
     */
    public function getIdOwner()
    {
        return $this->idOwner;
    }

    /**
     * @param User $idOwner
     */
    public function setIdOwner($idOwner)
    {
        $this->idOwner = $idOwner;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Lista
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lista")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id")
     * })
     */
    private $idList;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return string
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @param string $permission
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Lista
     */
    public function getIdList()
    {
        return $this->idList;
    }

    /**
     * @param Lista $idList
     */
    public function setIdList($idList)
    {
        $this->idList = $idList;
    }

    /**
     * @return User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param User $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }


}

