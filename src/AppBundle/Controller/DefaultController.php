<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swift_Mailer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    // akcja która jest odpowiedzalna na landing page, logika odpowiedzialna za wysyłanie maila z zaproszeniem do aplikacji
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return JsonResponse|Response
     */
    public function indexAction(Request $request, \Swift_Mailer $mailer)
    {
        if($request->request->get('email')) {
            $email = $request->request->get('email');

            if (!(empty($email)) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->sendInvite($email, $mailer);
                $arrData = ['output' => '<b style="color: black; font-weight: bold">Thank you</b>, invite send. Inivite new person!<span class="closebtn" onclick="document.getElementById(\'email-result-div\').style.display = \'none\'">&times;</span>'];
            } else {
                $arrData = ['output' => '<b style="color: black; font-weight: bold">Sorry</b>, something went wrong.<span class="closebtn" onclick="document.getElementById(\'email-result-div\').style.display = \'none\'">&times;</span>'];
            }
            return new JsonResponse($arrData);
        }

        return $this->render('shopping_list/landing.html.twig');
    }

    // akcja poniżej odpowiedziale za zwracanie statycznych stron
    /**
     * @Route("/thanksforregister", name="thx_register")
     * @return Response
     */
    public function thxRegisterAction()
    {
        return $this->render('shopping_list/thx_register.html.twig');
    }

    /**
     * @Route("/thanksforactivation", name="thx_activation")
     * @return Response
     */
    public function thxActivationAction()
    {
        return $this->render('shopping_list/thx_activation.html.twig');
    }

    /**
     * @Route("/oaplikacji", name="about")
     * @return Response
     */
    public function aboutAction()
    {
        return $this->render('shopping_list/footer/about.html.twig');
    }

    /**
     * @Route("/regulamin", name="terms")
     * @return Response
     */
    public function termsAction()
    {
        return $this->render('shopping_list/footer/terms.html.twig');
    }

    /**
     * @Route("/polityka", name="policy")
     * @return Response
     */
    public function policyAction()
    {
        return $this->render('shopping_list/footer/policy.html.twig');
    }

    /**
     * @Route("/kontakt", name="contact")
     * @return Response
     */
    public function contactAction()
    {
        return $this->render('shopping_list/footer/contact.html.twig');
    }

    /**
     * @Route("/autorzy", name="authors")
     * @return Response
     */
    public function authorsAction()
    {
        return $this->render('shopping_list/authors.html.twig');
    }

    // akcja ocpowiadająca za dodawanie klienta potrzebnego do działania oAuth2.0
//    /**
//     * @Route("/conf/add_client", name="addclient")
//     */
//    public function addclientAction()
//    {
//        $clientManager = $this->get('fos_oauth_server.client_manager.default');
//        $client = $clientManager->createClient();
//        $client->setAllowedGrantTypes(array('token', 'authorization_code', 'password'));
//        $clientManager->updateClient($client);
//        $output = sprintf("Added client with id: %s ====== Secret: %s",$client->getPublicId(),$client->getSecret());
//
//        return new Response($output);
//    }

    // metoda wysyłająca zaproszenie
    function sendInvite($email, $mailer){
        $message = (new \Swift_Message('Ktos zaprosił Cie do sprawdzenia aplikacji BuyMe'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView('Emails/invite.html.twig',
                    array('email' => $email)),
                'text/html'
            );
        $mailer->send($message);
    }
}
