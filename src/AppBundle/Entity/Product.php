<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product", indexes={@ORM\Index(name="product_fk0", columns={"id_list"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(name="quantity",
 *          column=@ORM\Column(
 *              name = "quantity",
 *              nullable = true
 *          )
 *      )
 * })
 */
class Product
{
    /**
     * @var bool
     *
     * @ORM\Column(name="product_status", type="boolean")
     */
    private $productStatus = false;

    /**
     * @var string
     *
     * @ORM\Column(name="product_name", type="string", length=40, nullable=false)
     */
    private $productName;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="string", length=10, nullable=false)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Lista
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lista", inversedBy="product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id")
     * })
     */
    private $idList;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=20)
     */
    private $unit;

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return bool
     */
    public function isProductStatus()
    {
        return $this->productStatus;
    }

    /**
     * @param bool $productStatus
     */
    public function setProductStatus($productStatus)
    {
        $this->productStatus = $productStatus;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param string $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Lista
     */
    public function getIdList()
    {
        return $this->idList;
    }

    /**
     * @param Lista $idList
     */
    public function setIdList($idList)
    {
        $this->idList = $idList;
    }
}

