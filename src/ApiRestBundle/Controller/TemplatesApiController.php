<?php

namespace ApiRestBundle\Controller;

use AppBundle\Entity\Lista;
use AppBundle\Entity\OwnList;
use AppBundle\Entity\Product;
use AppBundle\Entity\TemplateLists;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class TemplatesApiController extends FOSRestController
{
    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *     [
     *       {
     *         "id": 9,
     *         "template_products": [
     *           {
     *             "id": 22,
     *             "product_name": "filet białej ryby świeżej lub rozmrożone",
     *             "quantity": "1",
     *             "unit": "kilogram"
     *           },
     *           {
     *             "id": 23,
     *             "product_name": "cytryna",
     *             "quantity": "1",
     *             "unit": "sztuk"
     *           },
     *           {
     *             "id": 24,
     *             "product_name": "mąka pszenna",
     *             "quantity": "500",
     *             "unit": "mililitr"
     *           },
     *           {
     *             "id": 25,
     *             "product_name": "sól",
     *             "quantity": "30",
     *             "unit": "gram"
     *           },
     *           {
     *             "id": 26,
     *             "product_name": "olej słonecznikowy lub masło klarowane",
     *             "quantity": "300",
     *             "unit": "mililitr"
     *           }
     *         ],
     *         "template_name": "smażona ryba",
     *         "date": "2018-09-18T15:19:30+02:00"
     *       }
     *     ]
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Get the template of given group/recipe name",
     *     resource = true,
     *     parameters = {
     *     {
     *         "name" = "_template_name",
     *         "dataType" = "string",
     *         "required" = "true",
     *         "description" = "template list group/recipe name"
     *     }
     *     },
     *     section = "Templates",
     *     statusCodes = {
     *         200 = "Returned when template is successfully shown",
     *         404 = "Returned when the template is not found",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Get("/v1/get_templates")
     * @param Request $request
     * @return View
     */
    public function showTemplatesAction(Request $request)
    {
        $templateName = $request->get('_template_name');

        $templates = $this->getDoctrine()->getRepository(TemplateLists::class)->findTemplateByName($templateName);

        if (empty($templates))
        {
            return View::create("", Response::HTTP_NOT_FOUND);
        }

        return View::create($templates, Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *      ""
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Convert template list to actual active list",
     *     resource = true,
     *     requirements = {
     *     {
     *         "name" = "list_name",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new list name"
     *     },
     *     {
     *         "name" = "template_name",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "template list group/recipe name"
     *     }
     *     },
     *     section = "Templates",
     *     statusCodes = {
     *         200 = "Returned when template is successfully converted",
     *         404 = "Returned when template of given group/recipe name doesn't exist",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Post("/v1/convert_template")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function convertTemplateToListAction(Request $request, UserInterface $user)
    {
        $listName = $request->get('list_name');
        $templateName = $request->get('template_name');

        $em = $this->getDoctrine()->getManager();

        $template = $this->getDoctrine()->getRepository(TemplateLists::class)->findTemplateByName($templateName);

        if (empty($template))
        {
            return View::create("", Response::HTTP_NOT_FOUND);
        }

        $listFromTemplate = new Lista();
        $listFromTemplate->setListName($listName);
        $listFromTemplate->setData(new \DateTime());

        $email = $user->getEmail();
        $end = (string)rand(100, 9999);
        $hash = hash('sha256', $email.$end);

        $listFromTemplate->setHash($hash);
        $listFromTemplate->setListStatus("aktywna");

        $em->persist($listFromTemplate);
        $em->flush();

        $ownListForNewList = new OwnList();
        $ownListForNewList->setIdList($listFromTemplate);
        $ownListForNewList->setPermission('edit');
        $ownListForNewList->setIdUser($user);

        $em->persist($ownListForNewList);
        $em->flush();

        foreach ($template->getTemplateProducts() as $productFromTemplate)
        {
            $product = new Product();
            $product->setIdList($listFromTemplate);
            $product->setProductName($productFromTemplate->getProductName());
            $product->setQuantity($productFromTemplate->getQuantity());
            $product->setProductStatus(false);
            $product->setUnit($productFromTemplate->getUnit());

            $em->persist($product);
            $em->flush();
        }

        return View::create("", Response::HTTP_OK);
    }
}