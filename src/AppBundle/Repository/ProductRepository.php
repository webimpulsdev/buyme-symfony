<?php
/**
 * Created by PhpStorm.
 * User: piotr-rybski
 * Date: 04.09.18
 * Time: 10:47
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Product;
use Doctrine\ORM\Query;

class ProductRepository extends EntityRepository
{
    public function findByIdList($id)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:Product', 'p')
            ->innerJoin('AppBundle:Lista', 'l', 'WITH', 'p.idList = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();

        return $qb;
    }

    public function findProductsByIdList($id)
    {
        $fields = ['p.id', 'p.productName', 'p.productName', 'p.quantity', 'p.productStatus', 'p.unit'];

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select($fields)
            ->from('AppBundle:Lista', 'l')
            ->innerJoin('AppBundle:Product', 'p', 'WITH', 'l.id = p.idList')
            ->where('l.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();

        return $qb;
    }

    public function findProductsFromUserList($id)
    {
        $fields = ['l.id'];

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select($fields)
            ->addSelect("GROUP_CONCAT(p.productName ORDER BY p.productName SEPARATOR ', ') AS products")
            ->from('AppBundle:Lista', 'l')
            ->innerJoin('AppBundle:Product', 'p', 'WITH', 'l.id = p.idList')
            ->where('l.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();

        return $qb;
    }
}