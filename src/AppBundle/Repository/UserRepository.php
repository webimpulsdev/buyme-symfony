<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserRepository extends EntityRepository implements UserLoaderInterface {

    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
       return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function loadUserByUsernameAndGetId($username)
    {
        try {
            return $this->getEntityManager()
                ->createQueryBuilder()
                ->select('u.id')
                ->from('AppBundle:User', 'u')
                ->where('u.email = :email')
                ->setParameter('email', $username)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }

    public function loadProfileByLoggedUser($id)
    {
        $fields = ['u.name', 'u.surname', 'u.city', 'u.email, u.status'];

        try {
            return $this->getEntityManager()->createQueryBuilder()
                ->select($fields)
                ->from('AppBundle:User', 'u')
                ->where('u.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }
}