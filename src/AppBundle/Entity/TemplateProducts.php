<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TemplateProducts
 *
 * @ORM\Table(name="template_products")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateProductsRepository")
 */
class TemplateProducts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var TemplateLists
     *
     * @ORM\ManyToOne(targetEntity="TemplateLists", inversedBy="templateProducts")
     * @ORM\JoinColumn(name="id_template", referencedColumnName="id")
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="product_name", type="string", length=40)
     */
    private $productName;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="string", length=100)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=40)
     */
    private $unit;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productName.
     *
     * @param string $productName
     *
     * @return TemplateProducts
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName.
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return TemplateProducts
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unit.
     *
     * @param string $unit
     *
     * @return TemplateProducts
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit.
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return TemplateLists
     */
    public function getTemplate() : TemplateLists
    {
        return $this->template;
    }

    /**
     * @param TemplateLists $template
     */
    public function setTemplate(TemplateLists $template) : void
    {
        $this->template = $template;
    }
}
