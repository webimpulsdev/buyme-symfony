function copyLink() {
    /* Get the text field */
    var copyText = document.getElementById("copy");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    swal({
        text : "Link skopiowany do schowka",
        button : false,
        timer : 1000
    });
}


var $collectionHolder;
var $addNewItem = $('<a href="#" class="addProductButton row" style="padding: 10px 20px; text-align: center; background-color: #e8e8e8;"><i class="fas fa-plus" style="font-size: 30px; float:left; color: #17c107"></i><p class="plus" style="padding: 0">Dodaj produkt</p></a>');


$(document).ready(function () {


    function WriteToLog(oEvent){

        if(oEvent.keyCode == '61'){
            addNewForm()
        }
    }
    $(window).bind("keypress", function(oEvent){
        WriteToLog(oEvent);
    });



    $('.required_false').prop('required',false);

    $(":checkbox").labelauty({ icon: false, label: false });

    $( "#appbundle_lista_save" ).prop( "disabled", true );

    $collectionHolder = $('#product_list');

    $form = $('.form-control');

    $form.addClass();

    $collectionHolder.append($addNewItem);
    $collectionHolder.data('index', $collectionHolder.find('.panel').length)

    $collectionHolder.find('.panel-body').each(function () {
        addRemoveIcon($(this));
    });

    $addNewItem.click(function (e) {
        e.preventDefault();
        addNewForm();
    })




    var $inputs = $('#user').find('input');
    $inputs.each(function(){
        $(this).removeClass("form-control-none");
    });

    var $loginInputs = $('.user').find('input');
    $loginInputs.each(function(){
        $(this).removeClass("form-control-none");
    });

    //  checkBoxColor();
    disableSaveButton();
    inputsProduct();

    $('#user_Zarejestruj').parent().css("text-align", "center");

});

function addNewForm() {

    var prototype = $collectionHolder.data('prototype');

    var index = $collectionHolder.data('index');

    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);

    $collectionHolder.data('index', index+1);
    var $panel = $('<div class="panel newPanel"></div>');
    var $panelBody = $('<div class="panel-body newForm" style="text-align: center; margin: 0 auto;"></div>').append(newForm);

    $panel.append($panelBody);
    $addNewItem.before($panel);

    var $input = $('.newForm').find(':text');
    var $checkbox = $('.newForm').find(':checkbox');
    var $select = $('.newForm').find('select');
    var $deletePanel = $('.newForm').find('.row');

    $select.parent().addClass('col-lg-3 col-sm-3 col-5 center');
    $checkbox.parent().addClass('col-lg-1 col-sm-1 col-2 center added');
    $input.each(function () {
        if($(this).attr('placeholder') == 'Produkt' ){
            $(this).parent().addClass('col-lg-4 col-sm-4 col-10 center');
        }else if($(this).attr('placeholder') == 'Ilość'){
            $(this).parent().addClass('col-lg-2 col-sm-2 col-4 center');
        }
    })
    //.css( "float", "left" );

    $('.added').parent().addClass('row').css({"border-bottom" : "1px solid #e2e2e2", 'padding' : '10px 0'});
    $('.added').parent().addClass('new-row');

    var $rows = $('.new-row');

    $('.required_false').prop('required',false);

    $rows.each(function () {
        if($(this).find('.remove').length == 0){
            addRemoveIcon($(this));
        }
    })

    $(":checkbox").labelauty({ icon: false, label: false });

    // $added = $('.added');


    /* $added.each(function(index){
         if((index+1)%2 == 0 && (index+1)%3 != 0) {
             $(this).children().attr("placeholder", "Ilość");
         }else if((index+1)%3 == 0 ){
             $(this).children().attr("placeholder", "Jednostka");
         }else{
             $(this).children().attr("placeholder", "Produkt");
         }
         }) */
}

/**
 * adds a remove button to the panel that is passed in the parameter
 * @param $panel
 */
function addRemoveIcon ($panel) {
    var $removeButton = $('<div class="col-lg-2 col-sm-2 col-3 remove" style="text-align: center"><a href="#"><i class="fas fa-trash-alt" style="font-size:25px; color: red; margin-top: 8px;"></i></a></div>');
   // var $panelFooter = $('<div class="panel-footer"></div>').append($removeButton);
    $removeButton.click(function (e) {
        e.preventDefault();
        swal({
            title: "Potwierdź!",
            text: "Czy na pewno chcesz usunąć ten produkt?",
            icon: "warning",
            buttons: {
                canceled: {
                    text: "Nie usuwaj",
                    value: false
                },
                deleted: {
                    text: "Usuń",
                    value: true
                }
            }
        }).then((value) => {
            if (value) {
                $(e.target).parents('.panel').animate({
                    opacity: 0.25,
                    height: "toggle"
                }, 1, function() {
                    $(this).remove();
                });
            }
            $.ajax({
                url: window.location.href,
                type: "POST",
                dataType: "json",
                data: $('form').serialize(),
                async: true,
                success: function (data)
                {
                    console.log(data)
                }
            });
            console.log($('form').serialize());
        });
    });
    $panel.append($removeButton);
}

function disableSaveButton() {
    $('.saveRecord').attr('disabled', true);

    $('input').on("change", function(){
        //       if($(this).val() != ''){
        $('#appbundle_lista_save').attr('disabled', false);
        //       }else{
        //          $('#appbundle_lista_save').attr('disabled', true);
        //       }
    });

    $('.addProductButton').click( function(){
        $('#appbundle_lista_save').attr('disabled', false);
    });

    $('.remove').click( function(){
        $('#appbundle_lista_save').attr('disabled', false);
    });

    $('select').on("change", function(){
        //       if($(this).val() != ''){
        $('#appbundle_lista_save').attr('disabled', false);
        //      }else{
        //          $('#appbundle_lista_save').attr('disabled', true);
        //      }
    });
}

function addRemoveButton ($panel) {
    var $removeButton = $('<a href="#" class="btn btn-danger">Usuń</a>');
    //var $panelFooter = $('<div class="panel-footer"></div>').append($removeButton);
    $removeButton.click(function (e) {
        e.preventDefault();
        $(e.target).parents('.panel').animate({
            opacity: 0.25,
            height: "toggle"
        }, 1000, function() {
            $(this).remove();
        });
    });
    $panel.append($removeButton);
}

function inputsProduct() {

    $('select').addClass('form-control-none');

    var $inputTitle = $('.title').find('input[type=text]');
    var $inputBody = $('.formProduct').find('input[type=text]');

    var $inputsProduct = $('.formProduct').find('input[type=text]');


    $inputsProduct.each(function () {
        $(this).css({"background-color": "transparent", "border": "none"});

        $(this).parent().find('.edit').hide();
        $(this).parent().find('.check').hide();
    });

    $inputsProduct.focus(function() {
        $(this).css({ "background-color": "#FFFFFF", "border": "1px solid #666"});
        $(this).parent().find('.check').show();
        $(this).parent().find('.edit').hide();
    });

    $inputsProduct.focusout(function() {
        $(this).css({ "background-color": "transparent", "border": "none"});
        $(this).parent().find('.check').hide();
    });

    var $pencils = $('.formProduct').find('.edit');


    $inputsProduct.mousemove(
        function(){
            if(!($(this).is(":focus"))) {
                $(this).parent().find('.edit').show();
            }else{
                $(this).parent().find('.edit').hide();
            }
        });

    $pencils.mousemove(
        function(){
            if(!($(this).is(":focus"))) {
                $(this).show();
            }else{
                $(this).hide();
            }
        });

    $pencils.click(function () {
        $(this).prev().focus();
    })


    $inputsProduct.mouseout(
        function(){
            $(this).parent().find('.edit').hide();
        });

    $pencils.mouseout(
        function(){
            $(this).hide();
        });

}

function checkBoxColor(){
    var $status = $( ":checkbox" );
    $status.each(function () {
        //$(this).prop('indeterminate', true);
        if ($(this).prop("checked")) {
            $(this).parent().parent().parent().addClass('green');
        }else{
            $(this).parent().parent().parent().removeClass('green');
        }
    });
    $status.click(function (e) {
        $status.each(function () {
            if ($(this).prop("checked")) {
                $(this).parent().parent().parent().addClass('green');
            }else{
                $(this).parent().parent().parent().removeClass('green');
            }
        });
    });
}