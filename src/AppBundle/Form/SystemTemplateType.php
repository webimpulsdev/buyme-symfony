<?php
/**
 * Created by PhpStorm.
 * User: piotr-rybski
 * Date: 20.09.18
 * Time: 10:01
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class SystemTemplateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('add_to_active_lists', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning',
                    'id' => 'addToActiveLists'
                ]
            ])
        ;
    }
}