<?php

namespace ApiRestBundle\Controller;

use AppBundle\Entity\Lista;
use AppBundle\Entity\OwnList;
use AppBundle\Entity\Product;
use AppBundle\Entity\SharedList;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class ProductsApiController extends FOSRestController
{
    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *     {
     *       "id": 251,
     *       "listName": "Moja lista",
     *       "data": "2018-09-17T15:37:47+02:00",
     *       "products": [
     *         {
     *           "id": 321,
     *           "productName": "boczek",
     *           "quantity": "150",
     *           "productStatus": false,
     *           "unit": "gram"
     *         },
     *         {
     *           "id": 322,
     *           "productName": "cebula",
     *           "quantity": "1",
     *           "productStatus": false,
     *           "unit": "sztuk"
     *         },
     *         {
     *           "id": 323,
     *           "productName": "łodyga selera naciowego",
     *           "quantity": "2",
     *           "productStatus": false,
     *           "unit": "sztuk"
     *         },
     *         {
     *           "id": 324,
     *           "productName": "marchewka",
     *           "quantity": "1",
     *           "productStatus": false,
     *           "unit": "sztuk"
     *         },
     *         {
     *           "id": 325,
     *           "productName": "oliwa",
     *           "quantity": "100",
     *           "productStatus": false,
     *           "unit": "mililitr"
     *         },
     *         {
     *           "id": 326,
     *           "productName": "mielone mięso",
     *           "quantity": "500",
     *           "productStatus": false,
     *           "unit": "gram"
     *         },
     *         {
     *           "id": 333,
     *           "productName": "tarty parmezan",
     *           "quantity": "50",
     *           "productStatus": false,
     *           "unit": "gram"
     *         }
     *       ]
     *     }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Get short list info and products on it for logged user",
     *     resource = true,
     *     requirements = {
     *     {
     *         "name" = "_id",
     *         "dataType" = "integer",
     *         "requirement" = "\d",
     *         "description" = "list id"
     *     }
     *     },
     *     section = "Products",
     *     statusCodes = {
     *         200 = "Returned when list info and products on it are returned successfully",
     *         401 = "Returned when the list doesn't exists or selected list doesn't belong to logged user",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Get("/v1/get_products")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function getProductsAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id');

        $productsList = $this->getDoctrine()->getRepository(Lista::class)->findUserListById($id);
        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($id);
        $sharedListCheck = $this->getDoctrine()->getRepository(SharedList::class)->findSharedListByIdList($id);
        $sharedListOwnerCheck = $this->getDoctrine()->getRepository(SharedList::class)->findOwnerOfSharedListByIdList($id);

        if (!empty($sharedListCheck))
        {
            if ($checkList['id'] == $sharedListOwnerCheck['id_owner'] && $sharedListCheck['id_user'] == $user->getId())
            {
                $products = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($id);

                $productsList['products'] = $products;

                return View::create($productsList, Response::HTTP_OK);
            }

            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        if (empty($productsList) || $checkList['id'] != $user->getId())
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $products = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($id);

        $productsList['products'] = $products;

        return View::create($productsList, Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *     ""
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Delete single product from selected list",
     *     resource = true,
     *     requirements = {
     *     {
     *         "name" = "_id",
     *         "dataType" = "integer",
     *         "requirement" = "\d",
     *         "description" = "product id"
     *     },
     *     {
     *         "name" = "_id_list",
     *         "dataType" = "integer",
     *         "requirement" = "\d",
     *         "description" = "list id"
     *     }
     *     },
     *     section = "Products",
     *     statusCodes = {
     *         200 = "Returned when product on selected list wasn't found",
     *         204 = "Returned when product on selected list wasn't found",
     *         401 = "Returned when selected list doesn't belong to user or entered idList isn't the list on which product is added",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Delete("/v1/delete_product")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function deleteProductAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id');
        $idList = $request->get('_id_list');

        $em = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        if (empty($product))
        {
            return View::create("", Response::HTTP_NO_CONTENT);
        }

        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($product->getIdList()->getId());

        if ($checkList['id'] != $user->getId() || $idList != $product->getIdList()->getId())
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $em->remove($product);
        $em->flush();

        return View::create("", Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *     {
     *       "productName": "jogurt",
     *       "quantity": "100",
     *       "productStatus": true,
     *       "unit": "gram"
     *     }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Add single product to selected list",
     *     resource = true,
     *     requirements = {
     *     {
     *         "name" = "productName",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product name"
     *     },
     *     {
     *         "name" = "quantity",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product quantity"
     *     },
     *     {
     *         "name" = "productStatus",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product status"
     *     },
     *     {
     *         "name" = "unit",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product unit"
     *     }
     *     },
     *     parameters = {
     *     {
     *         "name" = "_id_list",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "id list where to add product"
     *     }
     *     },
     *     section = "Products",
     *     statusCodes = {
     *         200 = "Returned when product was added successfully",
     *         204 = "Returned when the productName wasn't entered",
     *         401 = "Returned when lust of given id doesn't exists or selected list doesn't belong to logged user",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Post("/v1/add_product")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function addProductAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id_list');

        $em = $this->getDoctrine()->getManager();

        $productName = $request->get('productName');
        $quantity = $request->get('quantity');
        $productStatus = $request->get('productStatus');
        $unit = $request->get('unit');

        if (empty($productName))
        {
            return View::create("", Response::HTTP_NO_CONTENT);
        }

        $idList = $this->getDoctrine()->getRepository(Lista::class)->find($id);

        if (empty($idList) || $idList->getListStatus() != 'aktywna')
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        if (empty($unit))
        {
            $unit = "sztuk";
        }

        if (empty($quantity))
        {
            $quantity = null;
        }

        if (empty($productStatus))
        {
            $productStatus = 0;
        }

        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($idList);

        if ($checkList['id'] != $user->getId())
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $productToAdd = new Product();
        $productToAdd->setIdList($idList);
        $productToAdd->setProductName($productName);
        $productToAdd->setQuantity($quantity);
        $productToAdd->setProductStatus($productStatus);
        $productToAdd->setUnit($unit);

        $em->persist($productToAdd);
        $em->flush();

        return View::create(['productName' => $productName, 'quantity' => $quantity, 'productStatus' => $productStatus, 'unit' => $unit], Response::HTTP_OK);
    }

    /**
     * #### Example of a successful response ####
     *
     * <details>
     *
     *   <summary>Show details</summary>
     *
     *     {
     *       "productName": "mleko",
     *       "quantity": "1",
     *       "productStatus": false,
     *       "unit": "litr"
     *     }
     *
     * </details>
     *
     * @ApiDoc(
     *     headers = {
     *     {
     *         "name" = "Authorization",
     *         "value" = "Bearer",
     *         "required" = true,
     *         "description" = "Put here authorization key"
     *     }
     *     },
     *     description = "Update single product to selected list",
     *     resource = true,
     *     requirements = {
     *     {
     *         "name" = "productName",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product name"
     *     },
     *     {
     *         "name" = "quantity",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product quantity"
     *     },
     *     {
     *         "name" = "productStatus",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product status"
     *     },
     *     {
     *         "name" = "unit",
     *         "dataType" = "string",
     *         "requirement" = "\s",
     *         "description" = "new product unit"
     *     }
     *     },
     *     parameters = {
     *     {
     *         "name" = "_id",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "id of product to update"
     *     },
     *     {
     *         "name" = "_id_list",
     *         "dataType" = "integer",
     *         "required" = "true",
     *         "description" = "id list where the product exists"
     *     }
     *     },
     *     section = "Products",
     *     statusCodes = {
     *         200 = "Returned when product was updated successfully",
     *         401 = "Returned when the list with given id doesn't belong to the logged user",
     *     },
     *     views = {"v1"}
     * )
     *
     * @Put("/v1/update_product")
     * @param Request $request
     * @param UserInterface $user
     * @return View
     */
    public function updateProductAction(Request $request, UserInterface $user)
    {
        $id = $request->get('_id');
        $idList = $request->get('_id_list');

        $productName = $request->get('productName');
        $quantity = $request->get('quantity');
        $productStatus = $request->get('productStatus');
        $unit = $request->get('unit');

        $em = $this->getDoctrine()->getManager();

        $checkList = $this->getDoctrine()->getRepository(OwnList::class)->findOwnListUserById($idList);
        $sharedListCheck = $this->getDoctrine()->getRepository(SharedList::class)->findSharedListByIdList($idList);
        $sharedListOwnerCheck = $this->getDoctrine()->getRepository(SharedList::class)->findOwnerOfSharedListByIdList($idList);

        if (!empty($sharedListCheck))
        {
            if ($checkList['id'] == $sharedListOwnerCheck['id_owner'] && $sharedListCheck['id_user'] == $user->getId())
            {
                $productToUpdate = $this->getDoctrine()->getRepository(Product::class)->find($id);
                $productToUpdate->setProductName($productName);
                $productToUpdate->setQuantity($quantity);
                $productToUpdate->setProductStatus($productStatus);
                $productToUpdate->setUnit($unit);

                $em->persist($productToUpdate);
                $em->flush();

                return View::create(['productName' => $productName, 'quantity' => $quantity, 'productStatus' => $productStatus, 'unit' => $unit], Response::HTTP_OK);
            }

            return View::create("", Response::HTTP_UNAUTHORIZED);
        }


        if ($checkList['id'] != $user->getId())
        {
            return View::create("", Response::HTTP_UNAUTHORIZED);
        }

        $productToUpdate = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $productToUpdate->setProductName($productName);
        $productToUpdate->setQuantity($quantity);
        $productToUpdate->setProductStatus($productStatus);
        $productToUpdate->setUnit($unit);

        $em->persist($productToUpdate);
        $em->flush();

        return View::create(['productName' => $productName, 'quantity' => $quantity, 'productStatus' => $productStatus, 'unit' => $unit], Response::HTTP_OK);
    }
}