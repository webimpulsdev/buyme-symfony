<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TemplateLists
 *
 * @ORM\Table(name="template_lists")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateListsRepository")
 */
class TemplateLists
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var TemplateProducts
     *
     * @ORM\OneToMany(targetEntity="TemplateProducts", mappedBy="template")
     */
    private $templateProducts;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=40)
     */
    private $templateName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct()
    {
        $this->templateProducts = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set templateName.
     *
     * @param string $templateName
     *
     * @return TemplateLists
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName.
     *
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return TemplateLists
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return TemplateProducts
     */
    public function getTemplateProducts()
    {
        return $this->templateProducts;
    }
}
