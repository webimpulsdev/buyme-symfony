var $collectionHolder;

var $addNewItem = $('<a href="#" style="margin: 10px;"><i class="fas fa-plus" style="font-size: 40px; float:left; color: #17c107"></i><p class="plus">Dodaj produkt</p></a>');

$(document).ready(function () {
    $collectionHolder = $('#product_list');

    $form = $('.form-control');

    $form.addClass();

    $collectionHolder.append($addNewItem);
    $collectionHolder.data('index', $collectionHolder.find('.panel').length)

    $collectionHolder.find('.panel-body').each(function () {
        addRemoveIcon($(this));
    });
    $addNewItem.click(function (e) {
        e.preventDefault();
        addNewForm();
    })

    $('.form-control').addClass( 'form-control-none');

    checkBoxColor();
});

function addNewForm() {

    var prototype = $collectionHolder.data('prototype');

    var index = $collectionHolder.data('index');

    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);

    $collectionHolder.data('index', index+1);
    var $panel = $('<div class="panel list-group-item newPanel"></div>');
    var $panelBody = $('<div class="panel-body newForm" style="text-align: center; margin: 0 auto;"></div>').append(newForm);
    $panel.append($panelBody);
    addRemoveButton($panel);
    $addNewItem.before($panel);

    var $checkBox = $('.newForm').find('.checkbox');
    $checkBox.hide();
    var $input = $('.newForm').find('.form-control');
    $input.parent().addClass('col-3 added');//.css( "float", "left" );
}

/**
 * adds a remove button to the panel that is passed in the parameter
 * @param $panel
 */
function addRemoveIcon ($panel) {
    var $removeButton = $('<div class="col-3" style="text-align: center"><a href="#"><i class="fas fa-trash-alt" style="font-size:25px; color: red; margin-top: 8px;"></i></a></div>');
   // var $panelFooter = $('<div class="panel-footer"></div>').append($removeButton);
    $removeButton.click(function (e) {
        e.preventDefault();
        $(e.target).parents('.panel').animate({
            opacity: 0.25,
            left: "+=50",
            height: "toggle"
        }, 1000, function() {
            $(this).remove();
        });

    });
    $panel.append($removeButton);
}

function addRemoveButton ($panel) {
    var $removeButton = $('<a href="#" class="btn btn-danger">Usuń</a>');
    //var $panelFooter = $('<div class="panel-footer"></div>').append($removeButton);
    $removeButton.click(function (e) {
        e.preventDefault();
        $(e.target).parents('.panel').animate({
            opacity: 0.25,
            left: "+=50",
            height: "toggle"
        }, 1000, function() {
            $(this).remove();
        });
    });
    $panel.append($removeButton);
}

function checkBoxColor(){
    var $status = $( ":checkbox" );
    $status.each(function () {
        //$(this).prop('indeterminate', true);
        if ($(this).prop("checked")) {
            $(this).parent().parent().parent().parent().parent().addClass('green');
        }else{
            $(this).parent().parent().parent().parent().parent().removeClass('green');
        }
    });
    $status.click(function (e) {
        $status.each(function () {
            if ($(this).prop("checked")) {
                $(this).parent().parent().parent().parent().parent().addClass('green');
            }else{
                $(this).parent().parent().parent().parent().parent().removeClass('green');
            }
        });
    });
}

