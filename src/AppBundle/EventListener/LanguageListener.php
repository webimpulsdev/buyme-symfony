<?php
/**
 * Created by PhpStorm.
 * User: Komp
 * Date: 04.09.2018
 * Time: 10:32
 */

namespace AppBundle\EventListener;
use Symfony\Component\HttpFoundation\Request;
use Negotiation\LanguageNegotiator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;

class LanguageListener
{



    public function onKernelRequest(GetResponseEvent $event)
    {

        $language = $event->getRequest()->headers->get('accept-language');
        $request = $event->getRequest();
        $languageToSet = explode(',', $language)[0];

        if(strpos($languageToSet, "pl") !== false){
            // ustaw jezyk polski

            $request->setLocale('pl');
        }elseif(strpos($languageToSet, "en") !== false){
            $request->setLocale('en');
        }
        else{
            // ustaw inny język

            $request->setLocale($request->getDefaultLocale());
        }

    }
}