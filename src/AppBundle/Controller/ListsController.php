<?php
/**
 * Created by PhpStorm.
 * User: bate
 * Date: 16.07.2018
 * Time: 12:01
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Lista;
use AppBundle\Entity\TemplateLists;
use AppBundle\Form\ListaType;
use AppBundle\Entity\OwnList;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListsController extends Controller
{
    // strona po zalogowaniu z widokiem list
    /**
     * @Route("/lists", name="lists")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allListsAction(Request $request)
    {
        // jesli ktos niezalogowany probuje dostac się po adresie - przekierowanie do strony logowania
        $user = $this->getUser();

        if (is_null($user)) {
            return $this->redirectToRoute('login');
        }

        $id = $user->getId();

        // pobranie list do których użytkownik ma dostęp
        $em =$this->getDoctrine()->getManager();
        $ownLists = $em->getRepository('AppBundle:OwnList')->findBy(['idUser' => $id]);

        $sharedLists = $em->getRepository('AppBundle:SharedList')->findSharedListByUserAndListStatus($id);

        // utworzenie tablicy aby zapisać właścicieli list udostępnionych użytkownikowi
        $owners = new ArrayCollection();

        foreach ($sharedLists as $sharedList) {
            $owners->add($sharedList->getIdOwner());
        }

        // pobranie list własnych
        $ownListsId = new ArrayCollection();
        foreach($ownLists as $ownList) {
            $ownListsId->add($ownList->getIdList());
        }

        // pobranie list udostępnionych
        $sharedListsId = new ArrayCollection();
        foreach($sharedLists as $ownList) {
            $sharedListsId->add($ownList->getIdList());
        }

        // podzial na listy aktywna i archiwalne na dwie tablice
        $activeListsOriginal = new ArrayCollection();
        $archiveListsOriginal = new ArrayCollection();
        $templateListsOriginal = new ArrayCollection();
        foreach($ownListsId as $ownId) {
            $lists = $em->getRepository('AppBundle:Lista')->findBy(['id' => $ownId]);
            foreach($lists as $list) {
                if($list->getListStatus() == 'aktywna') {
                    $activeListsOriginal->add($list);
                } else if ($list->getListStatus() == 'archiwum') {
                    $archiveListsOriginal->add($list);
                } else if ($list->getListStatus() == 'template'){
                    $templateListsOriginal->add($list);
                }
            }
        }

        // wczytanie szablonów systemowych
        $systemTemplateListsOriginal = new ArrayCollection();

        $systemTemplates = $this->getDoctrine()->getRepository(TemplateLists::class)->findAll();

        foreach ($systemTemplates as $systemTemplate)
        {
            $systemTemplateListsOriginal->add($systemTemplate);
        }

        // odwrocenie ich żeby były posortowane datami od najnowszej
        $archiveLists = new ArrayCollection(array_reverse($archiveListsOriginal->toArray()));
        $activeLists = new ArrayCollection(array_reverse($activeListsOriginal->toArray()));
        $templateLists = new ArrayCollection(array_reverse($templateListsOriginal->toArray()));
        $systemTemplatesToPage = new ArrayCollection(array_reverse($systemTemplateListsOriginal->toArray()));

        $em->flush();

        // paginator z gotowego bundle'a
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');

        $activeListsPagination = $paginator->paginate(
            $activeLists,
            $request->query->getInt('page-active', 1),
            10,
            ['pageParameterName' => 'page-active']

        );

        $archiveListsPagination = $paginator->paginate(
            $archiveLists,
            $request->query->getInt('page-archive', 1),
            10,
            ['pageParameterName' => 'page-archive']
        );

        $templateListsPagination = $paginator->paginate(
            $templateLists,
            $request->query->getInt('page-template', 1),
            5,
            ['pageParameterName' => 'page-template']
        );

        $systemTemplatesListsPagination = $paginator->paginate(
            $systemTemplatesToPage,
            $request->query->getInt('page-system-template', 1),
            5,
            ['pageParameterName' => 'page-system-template']
        );

        $sharedListsPagination = $paginator->paginate(
            $sharedLists,
            $request->query->getInt('pages-shared', 1),
            10,
            ['pageParameterName' => 'page-shared']
        );

        return $this->render("shopping_list/index.html.twig", ['archiveLists' => $archiveListsPagination, 'activeLists' => $activeListsPagination, 'sharedLists' => $sharedListsPagination, 'templateLists' => $templateListsPagination, 'owners' => $owners, 'systemTemplateLists' => $systemTemplatesListsPagination]);
    }

    // utwórz nową liste
    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createListAction(Request $request)
    {
        // zabezpieczenie
        $user = $this->getUser();
        if (is_null($user)) {
            return $this->redirectToRoute('login');
        }

        $em = $this->getDoctrine()->getManager();

        $lista = new Lista();

        // wygenerowanie hasha dla udostępniane po linku
        $email = $user->getEmail();
        $end = (string)rand(100, 9999);
        $hash = hash('sha256', $email.$end);

        $lista->setHash($hash);

        $originalProdukt = new ArrayCollection();
        foreach ($lista->getProduct() as $produkt) {
            $originalProdukt->add($produkt);
        }

        //utworzenie formularza
        $form = $this->createForm(ListaType::class, $lista);
        $form->handleRequest($request);

        //ustawienie danych do nowego obiektu listy
        if ($form->isSubmitted()) {
            $lista->setListStatus('aktywna');
            $lista->setData(new \DateTime());
            foreach ($originalProdukt as $produkt) {
                if ($lista->getProduct()->contains($produkt) === false) {
                    $em->remove($produkt);
                }
            }
            $em->persist($lista);
            $em->flush();

            $user = $this->getUser();

            // utworzenie nowego obiektu ownList, który łączy nową listę z użytkownikiem - właścicielem listy
            $ownList = new OwnList();
            $ownList->setIdList($lista);
            $ownList->setIdUser($user);
            $ownList->setPermission('edit');

            $em->persist($ownList);
            $em->flush();

            $idList = $ownList->getIdList()->getId();

            return $this->redirectToRoute('list', ['id' => $idList]);
        }

        return $this->render('shopping_list/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}