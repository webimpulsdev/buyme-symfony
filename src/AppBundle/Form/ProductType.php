<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productStatus', CheckboxType::class, array(
                'label'    => false,
                'required' => false))
            ->add('productName',
                TextType::class,
                    ['label' => false,
                        'attr' => array(
                            'placeholder' => 'Produkt',
                            'maxlength'=>"40"
                        )  ]
                )
            ->add('quantity',
                TextType::class,
                ['label' => false,
                    'attr' => array(
                        'class' => 'required_false',
                        'placeholder' => 'Ilość',
                        'maxlength'=>"10",
                        //'required' => false
                )]
            )
            ->add('unit', ChoiceType::class, array('label' => false, 'attr' => array('placeholder' => 'Ilość'),
                'choices' =>  array(
                        'landing.link158' => 'sztuk',
                        'landing.link159' => 'gram',
                        'landing.link160' => 'dekagram',
                        'landing.link161' => 'kilogram',
                        'landing.link162' => 'mililitr',
                        'landing.link163' => 'litr',
                        'landing.link164' => 'centymetr',
                        'landing.link165' => 'metr',
                )
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }
}
