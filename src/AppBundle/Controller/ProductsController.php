<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OwnList;
use AppBundle\Entity\SharedList;
use AppBundle\Entity\TemplateLists;
use AppBundle\Entity\TemplateProducts;
use AppBundle\Entity\User;
use AppBundle\Form\SharedListType;
use AppBundle\Form\SystemTemplateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Lista;
use AppBundle\Entity\Product;
use AppBundle\Form\ListaType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

class ProductsController extends Controller
{
    // strona po kliknięciu w wybraną listę
    /**
     * @Route("show/{id}", name="list")
     * @param Request $request
     * @param $id
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showListAction(Request $request, $id, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $lista Lista
         */
        $lista = $em->getRepository('AppBundle:Lista')->findOneBy(['id' => $id]);
        $hash = $lista->getHash();

        // zabezpieczenia, żeby nie można było poruszać się po dowolnych listach wpisując je w adres
        // czy użytkownik zalogowany
        $user = $this->getUser();

        if (is_null($user)) {
            return $this->redirectToRoute('login');
        }

        // jeśli użytkownik nie znajduje się w tabeli ownList lub sharedList podanej listy(czyli nie ma do niej dostępu) nastąpi przekierowanie do strony głównej
        $ownList = $em->getRepository('AppBundle:OwnList')->findOneBy(['idList' => $id]);
        $sharedLists = $em->getRepository('AppBundle:SharedList')->findBy(['idList' => $id]);

        $contains = false;

        foreach ($sharedLists as $list) {
            if($list->getIdUser() == $user) {
                $contains = true;
            }
        }

        if ((is_null($ownList) || !($ownList->getIdUser() == $user)) && (is_null($sharedLists) || !($contains))) {
            return $this->redirectToRoute('lists');
        }

        // pobranie produktów danej listy do tablicy
        $originalProdukt = new ArrayCollection();

        foreach ($lista->getProduct() as $produkt) {
            $originalProdukt->add($produkt);
        }

        // utworzenie formularza dla tej listy
        $form = $this->createForm(ListaType::class, $lista);
        $formPermissions = $this->createForm(SharedListType::class, $sharedLists);

        $form->handleRequest($request);
        $formPermissions->handleRequest($request);

        // jeśli użytkownik kliknie w przycik Archiwizuj/Przywróć to zmieni się status listy na przeciwny do wyjściowego
        if ($form->get('status')->isClicked()) {
            if($lista->getListStatus() == 'aktywna' ) {
                $lista->setListStatus('archiwum');
            } else {
                $lista->setListStatus('aktywna');
            }
        }

        // jeśli użytkownik kliknie przycisk Duplikuj lista zostanie zduplikowana
        if ($form->get('duplicate')->isClicked()) {
            $entityManager = $this->getDoctrine()->getManager();

            $duplicatedLista = new Lista();
            $duplicatedLista->setListName($lista->getListName());
            $duplicatedLista->setData(new \DateTime());
            $duplicatedLista->setHash(hash('sha256', $this->getUser()->getEmail().(string)rand(100, 9999)));
            $duplicatedLista->setListStatus('aktywna');

            $entityManager->persist($duplicatedLista);
            $entityManager->flush();

            $duplicatedOwnList = new OwnList();
            $duplicatedOwnList->setIdList($duplicatedLista);
            $duplicatedOwnList->setIdUser($ownList->getIdUser());
            $duplicatedOwnList->setPermission($ownList->getPermission());

            $entityManager->persist($duplicatedOwnList);
            $entityManager->flush();

            foreach ($lista->getProduct() as $product) {
                $duplicatedProduct = new Product();
                $duplicatedProduct->setIdList($duplicatedLista);
                $duplicatedProduct->setProductName($product->getProductName());
                $duplicatedProduct->setQuantity($product->getQuantity());
                $duplicatedProduct->setProductStatus(false);
                $duplicatedProduct->setUnit($product->getUnit());

                $entityManager->persist($duplicatedProduct);
                $entityManager->flush();
            }

            $this->addFlash('message-success', 'List duplicated.');

            return $this->redirectToRoute('lists', ['formPermissions' => $formPermissions->createView()]);
        }

        if ($form->get('delete')->isClicked())
        {
            $entityManagerToDeleteLista = $this->getDoctrine()->getManager();

            $listasToDelete = $entityManagerToDeleteLista->getRepository(Lista::class)->findAll();

            foreach ($listasToDelete as $listaToDelete) {
                if ($listaToDelete->getId() == $lista->getId())
                {
                    $listaToDelete->setListStatus('deleted');

                    $entityManagerToDeleteLista->persist($listaToDelete);
                    $entityManagerToDeleteLista->flush();
                }
            }

            $this->addFlash('message-success', 'List deleted.');

            return $this->redirectToRoute('lists');
        }

        if ($form->get('close_and_move')->isClicked())
        {
            $em = $this->getDoctrine()->getManager();

            $productsToCheck = $this->getDoctrine()->getRepository(Product::class)->findProductsByIdList($lista->getId());

            if (empty($productsToCheck))
            {
                $this->addFlash('message-danger', 'List without products cannot be closed and opened again. So it was automatically deleted!');

                $lista->setListStatus('deleted');
                $em->persist($lista);
                $em->flush();

                return $this->redirectToRoute('lists');
            }

            $listToArchive = new Lista();
            $listToArchive->setListName($lista->getListName());
            $listToArchive->setListStatus('archiwum');
            $listToArchive->setHash(hash('sha256', $this->getUser()->getEmail().(string)rand(100, 9999)));
            $listToArchive->setData(new \DateTime());

            $em->persist($listToArchive);
            $em->flush();

            $productsToAdd = $em->getRepository(Product::class)->findAll();

            $ownListToArchive = new OwnList();
            $ownListToArchive->setIdList($listToArchive);
            $ownListToArchive->setIdUser($ownList->getIdUser());
            $ownListToArchive->setPermission($ownList->getPermission());

            $em->persist($ownListToArchive);
            $em->flush();

            foreach ($productsToAdd as $product) {
                if ($product->isProductStatus() && $product->getIdList() == $lista)
                {
                    $product->setIdList($listToArchive);

                    $em->persist($product);
                    $em->flush();
                }
            }

            $this->addFlash('message-success', 'List closed and open again.');

            return $this->redirectToRoute('lists');
        }

        if ($form->get('to_template')->isClicked())
        {
            $em = $this->getDoctrine()->getManager();

            $lista->setListStatus('template');

            $em->persist($lista);
            $em->flush();

            $this->addFlash('message-success', 'List was successfully saved as template.');

            return $this->redirectToRoute('lists');
        }

        //proby stworzenie zmiany uprawnień
        if ($formPermissions->get('save_permissions')->isClicked())
        {
            $em = $this->getDoctrine()->getManager();
            $userId = $this->getDoctrine()->getManager()->getRepository(User::class)->loadUserByUsernameAndGetId($formPermissions['email_to_share']->getData());

            $newPermission = $formPermissions['permissions']->getData();
            $sharedList = $em->getRepository(SharedList::class)->findUserIdUser($userId['id'], $lista->getId());
            $sharedList->setPermission($newPermission);

            $em->persist($sharedList);
            $em->flush();

            $this->addFlash("message-success", "Permission changed.");
        }

        // kiedy użytkownik odeśle formularz, następuje porównanie produktów przed i po zmianach, jeśli użytkownik usunął produkt w widoku (obsługa w jquery - plik app.js), nastąpi usunięcie go z bazy
        if ($form->isSubmitted()) {
            foreach ($originalProdukt as $produkt) {
                if ($lista->getProduct()->contains($produkt) === false) {
                    $em->remove($produkt);
                }
            }
            $em->persist($lista);
            $em->flush();

            $this->addFlash("message-success", "Done.");
        }

        // udostępnianie listy
        // przycisk submit wysyłania listy na email
        $email_toSend = $request->get('email_toSend');

        if (!(empty($email_toSend))) {
            $this->sendProductsList($user->getName(), $email_toSend, $originalProdukt, $mailer);
            $this->addFlash("message", "List send to email.");
        }

        // udostępnianie listy użytkownikowi w systemie, sprawdza czy użytkownik o podanym adresi email istnieje, zwracany jest komunikat.
        if($request->get('email_toShare')) {
            $email = $request->get('email_toShare');
            $userShare = $em->getRepository('AppBundle:User')->findOneBy(['email' => $email]);


            if(is_null($userShare)) {
                $this->addFlash("message", "User not found.");
            } else {
                $owner = $this->getUser();

                $sharedList = new SharedList();

                $sharedList->setIdUser($userShare);
                $sharedList->setIdOwner($owner);
                $sharedList->setIdList($lista);
                $sharedList->setPermission('edit');

                $em->persist($sharedList);
                $em->flush();

                $this->sendSharedInfo($user->getName(), $email, $mailer);
                $this->addFlash("message-success", "List shared.");
            }

        }

        // zwroc widok listy tylko do odczytu i dla osoby ktora robi zakupy
        // return this->render
        return $this->render('shopping_list/products.html.twig', [
            'form' => $form->createView(),
            'formPermissions' => $formPermissions->createView(),
            'sharedLists' => $sharedLists,
            'id' => $id,
            'user' => $user,
            'hash' => $hash,
            'createdDate' => $lista->getData()->format('m/d/Y')
        ]);
    }

    /**
     * @Route("/template/show/{id}", name="template")
     * @param UserInterface $user
     * @param Request $request
     * @param $id
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showTemplateAction(UserInterface $user, Request $request, $id, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $lista Lista
         */
        $lista = $em->getRepository('AppBundle:Lista')->findOneBy(['id' => $id]);
        $hash = $lista->getHash();

        // zabezpieczenia, żeby nie można było poruszać się po dowolnych listach wpisując je w adres
        // czy użytkownik zalogowany
        $user = $this->getUser();

        if (is_null($user)) {
            return $this->redirectToRoute('login');
        }

        // jeśli użytkownik nie znajduje się w tabeli ownList lub sharedList podanej listy(czyli nie ma do niej dostępu) nastąpi przekierowanie do strony głównej
        $ownList = $em->getRepository('AppBundle:OwnList')->findOneBy(['idList' => $id]);

        if ((is_null($ownList) || !($ownList->getIdUser() == $user))) {
            return $this->redirectToRoute('lists');
        }

        // pobranie produktów danej listy do tablicy
        $originalProdukt = new ArrayCollection();

        foreach ($lista->getProduct() as $produkt) {
            $originalProdukt->add($produkt);
        }

        // utworzenie formularza dla tej listy
        $form = $this->createForm(ListaType::class, $lista);

        $form->handleRequest($request);

        // jeśli użytkownik kliknie w przycik Archiwizuj/Przywróć to zmieni się status listy na przeciwny do wyjściowego
        if ($form->get('status')->isClicked()) {
            if($lista->getListStatus() == 'aktywna' ) {
                $lista->setListStatus('archiwum');
            } else {
                $lista->setListStatus('aktywna');
            }
        }

        // jeśli użytkownik kliknie przycisk Duplikuj lista zostanie zduplikowana
        if ($form->get('duplicate')->isClicked()) {
            $entityManager = $this->getDoctrine()->getManager();

            $duplicatedLista = new Lista();
            $duplicatedLista->setListName($lista->getListName());
            $duplicatedLista->setData(new \DateTime());
            $duplicatedLista->setHash(hash('sha256', $this->getUser()->getEmail().(string)rand(100, 9999)));
            $duplicatedLista->setListStatus('aktywna');

            $entityManager->persist($duplicatedLista);
            $entityManager->flush();

            $duplicatedOwnList = new OwnList();
            $duplicatedOwnList->setIdList($duplicatedLista);
            $duplicatedOwnList->setIdUser($ownList->getIdUser());
            $duplicatedOwnList->setPermission($ownList->getPermission());

            $entityManager->persist($duplicatedOwnList);
            $entityManager->flush();

            foreach ($lista->getProduct() as $product) {
                $duplicatedProduct = new Product();
                $duplicatedProduct->setIdList($duplicatedLista);
                $duplicatedProduct->setProductName($product->getProductName());
                $duplicatedProduct->setQuantity($product->getQuantity());
                $duplicatedProduct->setProductStatus(false);
                $duplicatedProduct->setUnit($product->getUnit());

                $entityManager->persist($duplicatedProduct);
                $entityManager->flush();
            }

            $this->addFlash('message-success', 'List duplicated and added to active lists.');

            return $this->redirectToRoute('lists');
        }

        // kiedy użytkownik odeśle formularz, następuje porównanie produktów przed i po zmianach, jeśli użytkownik usunął produkt w widoku (obsługa w jquery - plik app.js), nastąpi usunięcie go z bazy
        if ($form->isSubmitted()) {
            foreach ($originalProdukt as $produkt) {
                if ($lista->getProduct()->contains($produkt) === false) {
                    $em->remove($produkt);
                }
            }
            $em->persist($lista);
            $em->flush();
        }

        return $this->render('shopping_list/user_template.html.twig', [
            'form' => $form->createView(),
            'id' => $id,
            'user' => $user,
            'hash' => $hash,
            'createdDate' => $lista->getData()->format('m/d/Y')
        ]);
    }

    /**
     * @Route("/system_template/show/{id}", name="system_templates")
     * @param UserInterface $user
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function systemTemplateListAction(UserInterface $user, Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $template = $em->getRepository(TemplateLists::class)->find($id);
        $templateProducts = $em->getRepository(TemplateProducts::class)->findBy(['template' => $template]);

        if (empty($template)) {
            return $this->redirectToRoute("homepage");
        }

        $form = $this->createForm(SystemTemplateType::class, $template);
        $form->handleRequest($request);

        if ($form->get('add_to_active_lists')->isClicked())
        {
            $list = new Lista();
            $list->setListName($template->getTemplateName());
            $list->setHash(hash('sha256', $this->getUser()->getEmail().(string)rand(100, 9999)));
            $list->setListStatus('aktywna');
            $list->setData(new \DateTime());

            $em->persist($list);
            $em->flush();

            $ownList = new OwnList();
            $ownList->setIdList($list);
            $ownList->setIdUser($user);
            $ownList->setPermission('edit');

            $em->persist($ownList);
            $em->flush();

            foreach ($templateProducts as $templateProduct)
            {
                $product = new Product();
                $product->setIdList($list);
                $product->setProductName($templateProduct->getProductName());
                $product->setUnit($templateProduct->getUnit());
                $product->setQuantity($templateProduct->getQuantity());
                $product->setProductStatus(0);

                $em->persist($product);
                $em->flush();
            }

            $this->addFlash('message-success', 'Recommended template was successfully added to active lists.');

            return $this->redirectToRoute('lists');
        }

        return $this->render('shopping_list/system_template.html.twig', [
            'products' => $templateProducts,
            'listName' => $template->getTemplateName(),
            'form' => $form->createView()
        ]);
    }

    // akcja odpowiada za udostępnianie listy przez link, dostępna dla każdego kto posiada prawidłowy link
    /**
     * @Route("read/{hash}", name="read_list")
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readListAction($hash)
    {
        $em = $this->getDoctrine()->getManager();
        $lista = $em->getRepository('AppBundle:Lista')->findOneBy(['hash' => $hash]);
        $produkty = $em->getRepository('AppBundle:Product')->findByIdList($lista->getId());

        if (empty($lista)) {
            return $this->redirectToRoute("homepage");
        }

        return $this->render('shopping_list/read_list.html.twig', [
            'products' => $produkty,
            'listName' => $lista->getListName()
        ]);
    }

    function sendProductsList($name, $email, $productsList, $mailer)
    {
        $message = (new \Swift_Message('Została wysłana do Ciebie lista zakupów!'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/product_list_to_email.html.twig',
                array('productsList' => $productsList, 'name' => $name)
                    ), 'text/html'
            );
        $mailer->send($message);
    }

    function sendSharedInfo($name, $email, $mailer)
    {
        $message = (new \Swift_Message('Udostępniono Ci listę zakupów!'))
            ->setFrom(['noreply@buyme-shopping.com' => 'BuyMe!'])
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'Emails/invite_to_shared_list.html.twig',
                    array('name' => $name)
                ), 'text/html'
            );
        $mailer->send($message);
    }
}